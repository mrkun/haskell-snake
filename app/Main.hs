module Main where

import Lib
import System.Console.ANSI(clearScreen)
import System.Random(mkStdGen,next,randomR,StdGen)

data Direction = U | D | L | R deriving (Read,Show)

data FieldState = FieldState {
      height :: Int
    , width :: Int
    , snakeLocations :: [(Int, Int)]
    , fruitLocation :: (Int, Int)
    , snakeDirection :: Direction
    , randomGen :: StdGen
}

simpleField :: FieldState
simpleField = FieldState {
      height = 10
    , width = 10
    , snakeLocations = [(5, 5), (5, 6), (5, 7)]
    , fruitLocation = (4, 5)
    , snakeDirection = U
    , randomGen = mkStdGen 0
}

instance Show FieldState where
    show f = unlines $ map showRow [0..height f] where
        showRow y = map (\x -> showCoords (x, y)) [0..width f]
        showCoords (x, y)
            | (x, y) == fruitLocation f = '#'
            | (x, y) == head (snakeLocations f) = 'o'
            | (x, y) `elem` snakeLocations f = '-'
            | otherwise = '.'

progressField :: FieldState -> Maybe FieldState
progressField f = if (elem newHead $ tail newSnake) then Nothing else Just f {
      snakeLocations = newSnake
    , fruitLocation = newFruit
    , randomGen = newRandomGen
}
    where
    newSnake :: [(Int, Int)]
    newSnake =
        let oldSnake = snakeLocations f in
        newHead : if hasEaten then oldSnake
                  else init oldSnake

    hasEaten :: Bool
    hasEaten = newHead == fruitLocation f

    newHead :: (Int, Int)
    newHead = let (dx, dy) = delta $ snakeDirection f
                  (x, y) = head $ snakeLocations f in
        (mod (x+dx) (width f + 1), mod (y+dy) (height f + 1))

    delta :: Direction -> (Int, Int)
    delta U = (0, -1)
    delta D = (0, 1)
    delta L = (-1, 0)
    delta R = (1, 0)

    newFruit :: (Int, Int)
    newFruit = if hasEaten
               then let (x, g1) = randomR (0, width f) $ randomGen f
                        (y, _)  = randomR (0, height f) g1
                        in (x, y)
               else fruitLocation f
    newRandomGen :: StdGen
    newRandomGen = if hasEaten
                   then snd . next . snd . next $ randomGen f
                   else randomGen f

readDirection :: Char -> Maybe Direction
readDirection 'D' = Just D
readDirection 'U' = Just U
readDirection 'L' = Just L
readDirection 'R' = Just R
readDirection _ = Nothing

main :: IO ()
main = go $ Just simpleField where
    go :: Maybe FieldState -> IO ()
    go f = do
        clearScreen

        putStr $ show f

        c <- getChar
        let newDirection = readDirection c

        go $ case newDirection of
             Just d -> (\f -> f {snakeDirection = d}) <$> f >>= progressField
             Nothing -> f >>= progressField

